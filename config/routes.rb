# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'wordles#show'

  get '/service-worker.js' => 'service_workers#service_worker'
end
