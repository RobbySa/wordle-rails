# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.3'

gem 'activerecord-import'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'get_process_mem'
gem 'health-monitor-rails', '~> 9.0'
gem 'kaminari', '~> 1.2'
gem 'tailwindcss-rails', '~> 2.0', '>= 2.0.7'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'

gem 'puma', '~> 4.1'
gem 'pundit', '~> 2.0'
gem 'rails', '~> 6.1.0'
gem 'service_actor'
gem 'slim-rails', '~> 3.2'
gem 'sqlite3', '>= 0.18'
gem 'ulid', '~> 1.3'
gem 'webpacker', '~> 5.0'

# Suggestions from the generated Gemfile below.

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 4.0.0'
end

group :development do
  gem 'brakeman', '~> 5.0', require: false
  gem 'bundler-audit', '~> 0.9', require: false
  gem 'capistrano', '~> 3.14', require: false
  gem 'capistrano3-puma', '~> 4.0', require: false
  gem 'capistrano-bundler', '~> 1.6', require: false
  gem 'capistrano-rails', '~> 1.6', require: false
  gem 'foreman', require: false
  gem 'guard', require: false
  gem 'guard-rspec', require: false
  gem 'guard-rubocop', require: false
  gem 'guard-shell', require: false
  gem 'i18n-tasks', '~> 0.9'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'rubocop-performance', '~> 1.5', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
  gem 'slim_lint', '~> 0.20', require: false
  gem 'solargraph'
  gem 'spring'
  gem 'spring-commands-cucumber', require: false
  gem 'spring-commands-rspec', require: false
  gem 'spring-watcher-listen'

  # Access an interactive console on exception pages or by calling 'console'
  # anywhere in the code.
  gem 'web-console', '>= 3.3.0'

  # Suggestions from the generated Gemfile below.

  # The listen gem does not support NFS-based filesystems such as that used by
  # Vagrant. See `config/environments/development.rb`.
  # gem 'listen', '>= 3.0.5', '< 3.2'
  # gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'cucumber-rails', require: false

  # No version here as this is an optional extra for cucumber-rails
  gem 'database_cleaner'

  gem 'email_spec', '~> 2.2'
  gem 'factory_bot_rails', '~> 5.1'
  gem 'rails-controller-testing'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 4.0'
  gem 'simplecov', '~> 0.18', require: false
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
